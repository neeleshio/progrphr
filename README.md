# Installation

#### You'll need git & npm installed on your computer.

# Usage

From your command line:

### Clone this repository
$ git clone https://neeleshio@bitbucket.org/neeleshio/progrphr.git

### Go into the repository
$ cd progrphr

### Install dependencies
$ npm install

### Run the server
$ npm start


#### Default port : 3000